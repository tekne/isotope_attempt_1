extern crate typed_generational_arena;

pub mod typing;
pub mod ir;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
