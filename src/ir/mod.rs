use typed_generational_arena::Index as ArenaIndex;
use typed_generational_arena::Arena;

use crate::typing::TypeRef;

pub type BlockIndex<R> = ArenaIndex<BasicBlock<R>>;
pub type ValueIndex<R> = ArenaIndex<Value<R>>;


#[derive(Debug)]
pub struct Body<R: TypeRef> {
    return_type : R,
    basic_blocks : Arena<BasicBlock<R>>,
    values : Arena<Value<R>>,
    parameters : Arena<Parameter<R>>,
    entry_block: BlockIndex<R>
}

impl<R: TypeRef> Body<R> {
    pub fn get_values(&self) -> &Arena<Value<R>> {
        &self.values
    }
    pub fn get_values_mut(&mut self) -> &mut Arena<Value<R>> {
        &mut self.values
    }
    pub fn get_parameters(&self) -> &Arena<Parameter<R>> {
        &self.parameters
    }
    pub fn get_parameters_mut(&mut self) -> &mut Arena<Parameter<R>> {
        &mut self.parameters
    }
    pub fn get_bbs(&self) -> &Arena<BasicBlock<R>> {
        &self.basic_blocks
    }
    pub fn get_bbs_mut(&mut self) -> &mut Arena<BasicBlock<R>> {
        &mut self.basic_blocks
    }
}

#[derive(Debug)]
pub struct BasicBlock<R: TypeRef> {
    todo: std::marker::PhantomData<R>
}

#[derive(Debug)]
pub struct Value<R: TypeRef> {
    todo: std::marker::PhantomData<R>
}

#[derive(Debug)]
pub struct Parameter<R: TypeRef> {
    value : ValueIndex<R>
}

impl<R: TypeRef> Parameter<R> {
    pub fn get_value(&self) -> ValueIndex<R> { self.value }
}
