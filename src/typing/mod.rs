use std::ops::Deref;

pub trait TypeRef: Deref<Target=Type<Self>> where Self: std::marker::Sized {}
impl<T: Deref<Target=Type<T>>> TypeRef for T {}

pub enum Type<R: TypeRef> {
    Scalar(ScalarType),
    Function(FunctionType<R>),
    Alias(TypeAlias<R>)
}

pub enum ScalarType {
    Bits(u32),
    Boolean
}

pub struct FunctionType<R: TypeRef> {
    domain : R,
    target : R
}

impl<R: TypeRef> FunctionType<R> {
    pub fn new<D: Into<R>, T: Into<R>>(domain : D, target : T) -> FunctionType<R> {
        FunctionType{ domain : domain.into(), target : target.into() }
    }
    pub fn borrow_domain(&self) -> &Type<R> { &self.domain }
    pub fn borrow_target(&self) -> &Type<R> { &self.target }
}

impl<R: TypeRef + Clone> FunctionType<R> {
    pub fn domain(&self) -> R { self.domain.clone() }
    pub fn target(&self) -> R { self.target.clone() }
}

pub struct TypeAlias<R: TypeRef> {
    aliased : R,
    alias : String
}

impl<R: TypeRef> TypeAlias<R> {
    pub fn new<A: Into<R>, N: Into<String>>(aliased : A, alias : N) -> TypeAlias<R> {
        TypeAlias{ aliased : aliased.into(), alias : alias.into() }
    }
    pub fn borrow_aliased(&self) -> &Type<R> { &self.aliased }
    pub fn name(&self) -> &str { &self.alias }
}

impl<R: TypeRef + Clone> TypeAlias<R> {
    pub fn aliased(&self) -> R { self.aliased.clone() }
}
